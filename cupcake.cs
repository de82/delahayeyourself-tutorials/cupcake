using System;
using System.Collections.Generic;

namespace cakes
{
    public class Cupcake : Cake
    {
        protected override List<string> Ingredients
        {
            get
            {
                List<string> ingredientsCupcake = base.Ingredients;
                ingredientsCupcake.Add("Farine");
                ingredientsCupcake.Add("Lait");
                ingredientsCupcake.Add("Sucre");
                ingredientsCupcake.Add("Oeufs");
                return ingredientsCupcake;
            }
        }
        public Cupcake() : base("Cupcake")
        {

        }
    }
}