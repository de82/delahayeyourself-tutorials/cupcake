﻿using System;
using System.Collections.Generic;

namespace cakes
{
    class Program
    {
        static void Main(string[] args)
        {


            Cake cupcake = new Cupcake();
            Cake cheesecake = new Cheesecake();
            Cake rbcupcake = new RainbowCupcake();

            Console.WriteLine(cupcake.Recipe());
            Console.WriteLine(cheesecake.Recipe());
            Console.WriteLine(rbcupcake.Recipe());
        }
    }
}
