using System;
using System.Collections.Generic;

namespace cakes
{
    public class RainbowCupcake : Cupcake
    {
        protected override List<string> Ingredients
        {
            get
            {
                List<string> ingredientsRBCupcake = base.Ingredients;
                ingredientsRBCupcake.Add("Unicorn poo");
                return ingredientsRBCupcake;
            }
        }
        public RainbowCupcake() : base()
        {
            this.name = "Rainbow Cupcake";
        }
    }
}