using System;
using System.Collections.Generic;
using System.Text;

namespace cakes
{
    public abstract class Cake
    {
        protected string name;
        protected virtual List<string> Ingredients
        {
            get
            {
                List<string> ingredients = new List<string>();

                return ingredients;
            }
        }

        public Cake(string name)
        {
            this.name = name;
        }

        public string Recipe()
        {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.AppendFormat("Cake: {0}, ingredients:\n", this.name);

            foreach (string ingredient in this.Ingredients)
            {
                stringBuilder.AppendFormat("\t- {0},\n", ingredient);
            }

            return stringBuilder.ToString();
        }
    }
}