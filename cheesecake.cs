using System;
using System.Collections.Generic;

namespace cakes
{
    public class Cheesecake : Cake
    {

        protected override List<string> Ingredients
        {
            get
            {
                List<string> ingredientsCheesecake = base.Ingredients;

                ingredientsCheesecake.Add("Farine");
                ingredientsCheesecake.Add("Lait");
                ingredientsCheesecake.Add("Sucre");
                ingredientsCheesecake.Add("Fromage");
                return ingredientsCheesecake;
            }
        }

        public Cheesecake() : base("Cheese cake")
        {

        }
    }
}